TEX := $(shell find content -type f -regex ".*\.tex")
PDF := $(TEX:.tex=.pdf)

# Où aller chercher les paquets LaTeX
export TEXINPUTS:=$(TEXINPUTS):$(PWD)/other/latex/

.PHONY: recursive build serve quick

all: $(PDF) build

.ONESHELL:
%.pdf: %.tex
	spix $<
	pdfautonup -o $@ $@ || true

.ONESHELL:
recursive: FORCE
	find content -name Makefile -execdir make \;

build: recursive $(PDF)
	lektor build --output-path public

serve: recursive $(PDF)
	lektor serve

quick:
	lektor build

FORCE:
