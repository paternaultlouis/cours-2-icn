⚠️ *Attention !*  Avec la réforme du bac 2021, l'enseignement d'ICN disparaît. Ce site ne sera donc plus mis à jour, et deviendra bientôt obsolète. L'ICN est plus ou moins remplacé par la [SNT](http://snt.ababsurdo.fr).

Ce dépôt contient mes supports de cours, devoirs, etc. pour une classe de seconde générale et technologique(dans l'enseignement public français) pour l'[informatique et création numérique](https://direct.eduscol.education.fr/philosophie/penser/enseigner-le-numerique/ressources-pour-enseigner-l-icn). Il ne contient que les sources, en LaTeX. Ce dépôt est aussi accessible sous forme de [de site web](http://icn.ababsurdo.fr).

Ces documents sont publiés sous license [Creative Commons by-sa 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr), qui en permet, entre autre, la modification et la réutilisation en classe, gratuitement ([j'explique ici](http://ababsurdo.fr/blog/pourquoi_publier_sous_licence_libre/) mon choix de diffusion libre).

Pour compiler le site web, il installer [lektor](http://getlektor.com), puis lancer ``lektor build``.

Pour me contacter, voir [la page dédiée](http://ababsurdo.fr/apropos/).
