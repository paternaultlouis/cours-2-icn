#! /usr/bin/env python3

import os
import mimetypes
from PIL import Image

os.chdir(os.path.dirname(os.path.abspath(__file__)))

def choix_image():
    print("#"*80)
    print("# Choix de l'image #")
    images = [name for name in sorted(os.listdir()) if str(mimetypes.guess_type(name)[0]).startswith("image")]
    for compteur in range(len(images)):
        print("[{}] {}".format(compteur, images[compteur]))
    while True:
        choix = input("Quelle image traiter ? ")
        try:
            if 0 <= int(choix) < len(images):
                print()
                return images[int(choix)]
        except ValueError:
            pass
        print("Veuillez choisir un nombre entre 0 et {}.".format(len(images)-1))

def docstring(fonction):
    if not fonction.__doc__.strip():
        raise Exception("La fonction '{}' n'a pas de docstring.".format(fonction.__name__))
    else:
        return fonction.__doc__.strip().split("\n")[0]

def extrait_rouge(nom):
    """MACHIN"""
    source = Image.open(nom).convert('RGB')
    largeur = source.width
    hauteur = source.height
    dest  = Image.new('RGB', (largeur, hauteur))

    for x in range(largeur):
        for y in range(hauteur):
            original = source.getpixel((x, y))
            nouvelle = (original[0], 0, 0)
            dest.putpixel((x, y), nouvelle)

    dest.save("TRUC.png")

ACTIONS = [
    extrait_rouge,
    ]

def choix_action():
    print("#"*80)
    print("# Choix de l'action #")
    for compteur in range(len(ACTIONS)):
        print("[{}] {}".format(compteur, docstring(ACTIONS[compteur])))
    while True:
        choix = input("Quelle action effectuer ? ")
        try:
            if 0 <= int(choix) < len(ACTIONS):
                print()
                return ACTIONS[int(choix)]
        except ValueError:
            pass
        print("Veuillez choisir un nombre entre 0 et {}.".format(len(ACTIONS)-1))

source = choix_image()
action = choix_action()

action(source)
