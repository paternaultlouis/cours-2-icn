def action_symetrie_gauchedroite(nom):
    """Effectue la symérie gauche-droite"""
    source = Image.open(nom).convert('RGB')
    largeur = source.width
    hauteur = source.height
    dest  = Image.new('RGB', (largeur, hauteur))

    for x in range(largeur):
        for y in range(hauteur):
            couleur = source.getpixel((x, y))
            dest.putpixel(
                    (..., y),
                    couleur
                    )

    dest.save("gauchedroite.png")
