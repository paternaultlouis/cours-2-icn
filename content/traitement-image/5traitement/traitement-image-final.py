#! /usr/bin/env python3

import os
import mimetypes
from PIL import Image

os.chdir(os.path.dirname(os.path.abspath(__file__)))

def choix_image():
    print("#"*80)
    print("# Choix de l'image #")
    images = [name for name in sorted(os.listdir()) if str(mimetypes.guess_type(name)[0]).startswith("image")]
    for compteur in range(len(images)):
        print("[{}] {}".format(compteur, images[compteur]))
    while True:
        choix = input("Quelle image traiter ? ")
        try:
            if 0 <= int(choix) < len(images):
                print()
                return images[int(choix)]
        except ValueError:
            pass
        print("Veuillez choisir un nombre entre 0 et {}.".format(len(images)-1))

def docstring(fonction):
    if not fonction.__doc__.strip():
        raise Exception("La fonction '{}' n'a pas de docstring.".format(fonction.__name__))
    else:
        return fonction.__doc__.strip().split("\n")[0]

def lit_couleur():
    while True:
        couleur = input("Couleur ? ").strip()
        if couleur == "blanc":
            return (255, 255, 255)
        if couleur == "noir":
            return (0, 0, 0)
        if couleur == "rouge":
            return (255, 0, 0)
        if couleur == "vert":
            return (0, 255, 0)
        if couleur == "bleu":
            return (0, 0, 255)

def action_noir_et_blanc(nom):
    """Convertit l'image en noir et blanc."""
    source = Image.open(nom).convert('RGB')
    largeur = source.width
    hauteur = source.height
    dest  = Image.new('RGB', (largeur, hauteur))

    for x in range(largeur):
        for y in range(hauteur):
            couleur = source.getpixel((x, y))
            moyenne = (couleur[0] + couleur[1] + couleur[2])//3
            dest.putpixel((x, y), (moyenne, moyenne, moyenne))

    dest.save("nb.png")

def action_extrait_rouge(nom):
    """Extrait la couleur rouge de l'image"""
    source = Image.open(nom).convert('RGB')
    largeur = source.width
    hauteur = source.height
    dest  = Image.new('RGB', (largeur, hauteur))

    for x in range(largeur):
        for y in range(hauteur):
            original = source.getpixel((x, y))
            nouvelle = (original[0], 0, 0)
            dest.putpixel((x, y), nouvelle)

    dest.save("rouge.png")

def action_extrait_vert(nom):
    """Extrait la couleur vert de l'image"""
    source = Image.open(nom).convert('RGB')
    largeur = source.width
    hauteur = source.height
    dest  = Image.new('RGB', (largeur, hauteur))

    for x in range(largeur):
        for y in range(hauteur):
            original = source.getpixel((x, y))
            nouvelle = (0, original[1], 0)
            dest.putpixel((x, y), nouvelle)

    dest.save("vert.png")

def action_extrait_bleu(nom):
    """Extrait la couleur bleu de l'image"""
    source = Image.open(nom).convert('RGB')
    largeur = source.width
    hauteur = source.height
    dest  = Image.new('RGB', (largeur, hauteur))

    for x in range(largeur):
        for y in range(hauteur):
            original = source.getpixel((x, y))
            nouvelle = (0, 0, original[2])
            dest.putpixel((x, y), nouvelle)

    dest.save("bleu.png")

def action_symetrie_gauchedroite(nom):
    """Effectue la symérie gauche-droite"""
    source = Image.open(nom).convert('RGB')
    largeur = source.width
    hauteur = source.height
    dest  = Image.new('RGB', (largeur, hauteur))

    for x in range(largeur):
        for y in range(hauteur):
            dest.putpixel((largeur-x-1, y), source.getpixel((x, y)))

    dest.save("gauchedroite.png")

def action_symetrie_hautbas(nom):
    """Effectue la symérie haut-bas"""
    source = Image.open(nom).convert('RGB')
    largeur = source.width
    hauteur = source.height
    dest  = Image.new('RGB', (largeur, hauteur))

    for x in range(largeur):
        for y in range(hauteur):
            dest.putpixel((x, hauteur-y-1), source.getpixel((x, y)))

    dest.save("hautbas.png")

def action_ajouter_cadre(nom):
    """Ajouter un cadre"""
    source = Image.open(nom).convert('RGB')
    largeur = source.width
    hauteur = source.height
    couleur = lit_couleur()
    epaisseur = int(input("Épaisseur du cadre (en pixels) ? "))
    dest  = Image.new('RGB', (largeur + 2*epaisseur, hauteur + 2*epaisseur))

    for x in range(largeur):
        for y in range(hauteur):
            dest.putpixel((x+epaisseur, y+epaisseur), source.getpixel((x, y)))
    for x in range(largeur+2*epaisseur):
        for y in range(hauteur+2*epaisseur):
            if x<=epaisseur or x>=largeur+epaisseur or y<=epaisseur or y>=hauteur+epaisseur:
                dest.putpixel((x, y), couleur)

    dest.save("cadre.png")

def action_reduire1(nom):
    """Réduit l'image de moitié (version rapide)"""
    source = Image.open(nom).convert('RGB')
    largeur = source.width
    hauteur = source.height
    dest  = Image.new('RGB', (largeur//2, hauteur//2))

    for x in range(largeur//2):
        for y in range(hauteur//2):
            dest.putpixel((x, y), source.getpixel((2*x, 2*y)))

    dest.save("petit1.png")

def action_reduire2(nom):
    """Réduit l'image de moitié (version plus précise)"""
    source = Image.open(nom).convert('RGB')
    largeur = source.width
    hauteur = source.height
    dest  = Image.new('RGB', (largeur//2, hauteur//2))

    for x in range(largeur//2):
        for y in range(hauteur//2):
            dest.putpixel((x, y), tuple(sum(l)//4 for l in zip(
                source.getpixel((2*x, 2*y)),
                source.getpixel((2*x, 2*y+1)),
                source.getpixel((2*x+1, 2*y)),
                source.getpixel((2*x+1, 2*y+1)),
                )))

    dest.save("petit2.png")

def action_eclaircir(nom):
    """Éclaircit l'image"""
    source = Image.open(nom).convert('RGB')
    largeur = source.width
    hauteur = source.height
    dest  = Image.new('RGB', (largeur, hauteur))

    for x in range(largeur):
        for y in range(hauteur):
            couleur = source.getpixel((x, y))
            dest.putpixel((x, y), (
                couleur[0]//2+128,
                couleur[1]//2+128,
                couleur[2]//2+128,
                ))

    dest.save("eclaircie.png")

def action_assombrir(nom):
    """Assombrit l'image"""
    source = Image.open(nom).convert('RGB')
    largeur = source.width
    hauteur = source.height
    dest  = Image.new('RGB', (largeur, hauteur))

    for x in range(largeur):
        for y in range(hauteur):
            couleur = source.getpixel((x, y))
            dest.putpixel((x, y), (
                couleur[0]//2,
                couleur[1]//2,
                couleur[2]//2,
                ))

    dest.save("assombrie.png")

def action_permuter(nom):
    """Permuter les couleurs"""
    source = Image.open(nom).convert('RGB')
    largeur = source.width
    hauteur = source.height
    dest  = Image.new('RGB', (largeur, hauteur))

    for x in range(largeur):
        for y in range(hauteur):
            couleur = source.getpixel((x, y))
            dest.putpixel((x, y), (
                couleur[1],
                couleur[2],
                couleur[0],
                ))

    dest.save("permutee.png")

def action_rotation90(nom):
    """Pivoter la photo de 90° vers la gauche"""
    source = Image.open(nom).convert('RGB')
    largeur = source.width
    hauteur = source.height
    dest  = Image.new('RGB', (hauteur, largeur))

    for x in range(hauteur):
        for y in range(largeur):
            dest.putpixel((x, y), source.getpixel((largeur-1-y, x)))

    dest.save("rotation90.png")

def action_contraste(nom):
    """Augmenter le contraste"""
    source = Image.open(nom).convert('RGB')
    largeur = source.width
    hauteur = source.height
    dest  = Image.new('RGB', (largeur, hauteur))

    for x in range(largeur):
        for y in range(hauteur):
            couleur = list(source.getpixel((x, y)))
            for compteur in range(len(couleur)):
                if couleur[compteur] < 128:
                    couleur[compteur] = couleur[compteur]//2
                else:
                    couleur[compteur] = couleur[compteur]//2+128
            dest.putpixel((x, y), tuple(couleur))

    dest.save("contraste.png")

def action_inverse(nom):
    """Inverser les couleurs"""
    source = Image.open(nom).convert('RGB')
    largeur = source.width
    hauteur = source.height
    dest  = Image.new('RGB', (largeur, hauteur))

    for x in range(largeur):
        for y in range(hauteur):
            couleur = source.getpixel((x, y))
            dest.putpixel((x, y), (
                255-couleur[0],
                255-couleur[1],
                255-couleur[2],
                ))

    dest.save("inverse.png")

def action_psychedelique(nom):
    """Produit une version psychédélique de l'image"""
    source = Image.open(nom).convert('RGB')
    largeur = source.width
    hauteur = source.height
    dest  = Image.new('RGB', (largeur, hauteur))

    for x in range(largeur):
        for y in range(hauteur):
            couleur = list(source.getpixel((x, y)))
            for compteur in range(len(couleur)):
                couleur[compteur] = 16*(couleur[compteur]%16)
            dest.putpixel((x, y), tuple(couleur))

    dest.save("psychedelique.png")

def action_reduit_couleurs(nom):
    """Réduire le nombre de couleurs"""
    source = Image.open(nom).convert('RGB')
    largeur = source.width
    hauteur = source.height
    dest  = Image.new('RGB', (largeur, hauteur))

    for x in range(largeur):
        for y in range(hauteur):
            couleur = list(source.getpixel((x, y)))
            for compteur in range(len(couleur)):
                couleur[compteur] = 32*(couleur[compteur]//32)
            dest.putpixel((x, y), tuple(couleur))

    dest.save("reduit_couleurs.png")

ACTIONS = [
    action_noir_et_blanc,
    action_extrait_rouge,
    action_extrait_vert,
    action_extrait_bleu,
    action_symetrie_gauchedroite,
    action_symetrie_hautbas,
    action_ajouter_cadre,
    action_reduire1,
    action_reduire2,
    action_eclaircir,
    action_assombrir,
    action_permuter,
    action_rotation90,
    action_contraste,
    action_psychedelique,
    action_reduit_couleurs,
    action_inverse,
    ]

def choix_action():
    print("#"*80)
    print("# Choix de l'action #")
    for compteur in range(len(ACTIONS)):
        print("[{}] {}".format(compteur, docstring(ACTIONS[compteur])))
    while True:
        choix = input("Quelle action effectuer ? ")
        try:
            if 0 <= int(choix) < len(ACTIONS):
                print()
                return ACTIONS[int(choix)]
        except ValueError:
            pass
        print("Veuillez choisir un nombre entre 0 et {}.".format(len(ACTIONS)-1))

source = choix_image()
action = choix_action()

action(source)
