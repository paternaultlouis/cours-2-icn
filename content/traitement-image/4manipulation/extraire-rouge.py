from PIL import Image

image = Image.open('gere.jpg')

for x in range(image.width):
    for y in range(image.height):
        couleur = image.getpixel((x, y))
        nouvelle = (couleur[0], 0, 0)
        image.putpixel((x, y), nouvelle)

image.save("gere-mono.png")
