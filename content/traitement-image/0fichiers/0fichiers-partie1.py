#!/bin/env python3

from base64 import b64decode
import os

DIRNAME = os.path.dirname(os.path.abspath(__file__))
EXPECTED = os.path.join(os.getenv("HOME"), "Bureau", "TP0")

BRAVO = b'CiAgX19fXyAgICAgICAgICAgICAgICAgICAgICAgICBfIAogfCAgXyBcICAgICAgICAgICAgICAgICAgICAgICB8IHwKIHwgfF8pIHxfIF9fIF9fIF9fXyAgIF9fX19fICAgfCB8CiB8ICBfIDx8ICdfXy8gX2AgXCBcIC8gLyBfIFwgIHwgfAogfCB8XykgfCB8IHwgKF98IHxcIFYgLyAoXykgfCB8X3wKIHxfX19fL3xffCAgXF9fLF98IFxfLyBcX19fLyAgKF8pCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAo='
RATE = b'CiAgX19fX18gICAgICAgXyAgICBfXyAgICAgICAKIHwgIF9fIFwgICAgIHwgfCAgL18vICAgICAgIAogfCB8X18pIHxfXyBffCB8XyBfX18gICAgICAgCiB8ICBfICAvLyBfYCB8IF9fLyBfIFwgICAgICAKIHwgfCBcIFwgKF98IHwgfHwgIF9fL18gXyBfIAogfF98ICBcX1xfXyxffFxfX1xfX18oX3xffF8pCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAK'

print("Ce fichier est enregistré dans le dossier :", DIRNAME)
print("Il devrait être situé sur le bureau, dans le dossier `TP0` ({}).".format(EXPECTED))

if DIRNAME == EXPECTED:
	print(b64decode(BRAVO).decode("utf8"))
else:
	print(b64decode(RATE).decode("utf8"))

