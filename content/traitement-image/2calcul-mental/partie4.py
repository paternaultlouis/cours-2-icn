from random import randint

score = 0

for i in range(10):
	# Choix de deux nombres aléatoires
	a = randint(1, 10)
	b = randint(1, 10)

	# Calcul de la solution
	solution = a + b

	# Afficher la question
	print("Calculer", a, "+", b)

	reponse = int(input("Réponse ? "))

	if reponse == solution:
		print("Correct")
		score += 1
	else:
		print("Perdu. La bonne réponse était :", solution)

	print()

print("Terminé")
print("Score :", score, "/ 10")
