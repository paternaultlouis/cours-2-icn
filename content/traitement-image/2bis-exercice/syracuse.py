#!/usr/bin/env python3

nombre = int(input("Nombre de départ ? "))

for i in range(10):
    if nombre % 2 == 0:
        nombre = nombre // 2
    else:
        nombre = 3 * nombre + 1
    print(nombre)
