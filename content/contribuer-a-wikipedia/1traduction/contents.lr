title: Traduction
---
body:

Dans cette séquence, vous allez contribuer à Wikipédia. Écrire un nouvel article n'est pas simple : il faut trouver un sujet qui n'a pas encore été traité, trouver des sources pertinentes, et synthétiser ces sources en un nouvel article. Nous allons faire une contribution plus simple : la traduction d'un article déjà existant dans une autre langue.

1. Choisissez un article à traduire.

  - Cet article doit provenir de Wikipédia.
  - Cet article doit être écrit dans une autre langue que le français, que vous lisez à peu près (c'est-à-dire l'anglais, ou une autre langue apprise au lycée, ou une autre langue que vous parlez en famille) ;
  - Cet article doit, au choix :
    - ne pas exister en français ;
    - être beaucoup moins complet en français.
  - Cet article ne doit pas être trop long (nous n'allons consacrer que deux heures à cette traduction).

2. Ouvrez un éditeur de texte (LibreOffice Writer par exemple), et écrivez une traduction de l'article.

  - Il est *interdit* d'utiliser un traducteur automatique (comme [reverso](http://reverso.net) ou [Google traduction](http://translate.google.fr)) : ces outils peuvent être parfois utiles pour dépanner, mais ne produisent pas un texte d'une assez bonne qualité pour Wikipédia.
  - Vous pouvez en revanche utiliser un dictionnaire bilingue en ligne, tel que [le Grand Dictionnaire Terminologique](http://www.granddictionnaire.com/) (principalement pour l'anglais), le dictionnaire [Larousse](https://larousse.fr/dictionnaires/bilingues), ou [WordReference](http://wordreference.com/).
  - Ne traduisez pas que le texte : traduisez aussi (si nécessaire) les titres, les encarts, les légendes des illustrations, les notes de bas de page, etc.
  - N'oubliez pas de mettre le lien de l'article que vous traduisez en tête de votre traduction.

3. Rendez-moi ce fichier sur Pronote.
