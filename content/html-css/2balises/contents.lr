title: Plus de balises
---
body:

Reprenez la page web écrite lors du TP précédent.

## Image

Cette page web est un peu terne. Rechercher sur internet comment inclure une image dans une page web, et ajouter une image pertinente à votre page. Par exemple :

- en français, vous pouvez rechercher `inclure image html`. Les sites suivants (parmi de nombreux autres) donnent souvent des réponses de qualité : http://openclassrooms.com, http://developer.mozilla.org, etc.
- en anglais, vous pouvez rechercher `include image html`. Les sites suivants (parmi de nombreux autres) donnent souvent des réponses de qualité : http://www.w3schools.com, http://stackoverflow.com, etc.

Pour le moment, votre image ne sera sans doute pas joliment intégrée à la page (trop grosse, trop petite, trop à gauche ou à droite, sans légende, etc.) ; ce n'est pas grave, nous nous occuperons de cela plus tard.

## Liens externes

Un bon site web a du contenu de qualité, mais aussi des liens vers d'autres sites web de qualité.

1. Recherchez sur internet comment ajouter un lien vers une page d'un autre site web.
2. Ajouter sur votre page web un lien vers un autre site web pertinent (le site web de votre personnage, sa chaîne Youtube, un article qui parle d'elle, la page Wikipédia de son auteur, etc.).
3. Faites en sorte que ce lien soit intégré au texte.

    - *Bon exemple :* Mae Jemison est une scientifique, docteure et [astronaute](https://www.jsc.nasa.gov/Bios/htmlbios/jemison-mc.html) américaine. C'est la première vraie astronaute à avoir joué dans [un épisode de Star Trek](http://www.startrek.com/database_article/palmer-lieutenant).
    - *Mauvais exemple :* Mae Jemison est une scientifique, docteure et astronaute américaine (voir sa biographie : https://www.jsc.nasa.gov/Bios/htmlbios/jemison-mc.html). C'est la première vraie astronaute à avoir joué dans un épisode de Star Trek : http://www.startrek.com/database_article/palmer-lieutenant.

## Liens internes

Un site web est souvent composé de plusieurs pages, et permet de naviguer entre elles.

1. Créer deux nouvelles pages pour votre site web. Ces pages sont des fichiers HTML situés dans le même dossier que le votre page actuelle :

    - la page principale de votre site web, appelée `index.html` ;
    - une page pour un second personnage.

  Ne mettez dans ces deux pages que le minimum pour avoir une page HTML valide : elles seront complétées plus tard.

2. Ajoutez des liens pour que :

  - de la page principale, il soit possible d'aller vers chacune des deux pages des personnages ;
  - de chacune des pages des personnages, il soit possible de revenir à la page principale.

## Listes et Tableau

Recherchez sur internet comment faire les choses suivantes.

1. Ajoutez une liste (numérotée ou non) à une de vos pages web (en utilisant une balise `<ol>` ou `<ul>`).
2. Ajoutez un tableau à une de vos pages web (en utilisant une balise `<table>`).

<!--
## Serveur web

1. Copiez l'ensemble de votre site web (les trois fichiers `html`, et éventuellement l'image) dans le dossier `~/public_html` (où `~` désigne la racine de votre répertoire personnel). Il sera peut être nécessaire de créer ce répertoire `~/public_html` d'abord.
2. Dans Firefox, consultez l'adresse : http://HOSTNAME.local/~USER (où `HOSTNAME` est le nom de votre machine, et `USER` votre nom d'utilisateur). Vous devriez voir votre site web.
3. En changeant le numéro de l'adresse ci-dessus, vous pouvez voir les sites webs de vos camarades de classe.
-->

## Validation

Vérifiez que les trois pages de votre site web ne produisent aucune erreur sur http://validator.w3.org.

## Évaluation

Complétez la [fiche d'évaluation](evaluation2.pdf), copiez votre travail dans le dossier partagé, rendez-moi la fiche d'évaluation, et passez au TP suivant.
